"""Calculates the weak dependency between heart rate and stroke volume"""
# Python imports
import os
import sys
import logging
import json
from functools import lru_cache

# Module imports
import numpy as np
import torch
from torch.optim import LBFGS
from scipy.stats import nct
import specfunc as sf
import nevergrad as ng
from tqdm import tqdm

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
import distrel

Pi = torch.acos(torch.zeros(1)) * 2
BIGNUM = 2 ** 16 - 1


def loghyp1f1(a, b, z):
    """Wrap around the specfunc.hyp1f1ln function."""
    z_np = z.detach().numpy().reshape(1, -1).astype(np.float64)
    b_np = np.zeros_like(z_np) + b
    a_np = np.zeros_like(z_np) + a
    out = torch.tensor(sf.hyp1f1ln(a_np, b_np, z_np), dtype=torch.double)
    return torch.nan_to_num(out, nan=-BIGNUM)


@lru_cache
def nct_pdf(x, df, nc, norm=True):
    """Non-central t distribution probability density function.

    There are problems with the scipy implementation so this function fixes those by
    using mpmath. Most of the function is also implemented in pytorch.
    Maths taken from: https://en.wikipedia.org/wiki/Noncentral_t-distribution#Probability_density_function

    Args:
        x (torch tensor) : Values to evaluate the pdf at.
        df (int) : Degrees of freedom.
        nc (float) : Non-centrallity parameter.

    Returns:
        pdf (torch.tensor) : Probability density at each x.
    """
    n = torch.tensor(df)
    nc = torch.tensor(nc)

    x2 = x ** 2
    mu2 = nc ** 2
    mu2x2 = mu2 * x2
    _2 = torch.tensor(2)

    # Student T(x; mu = 0)
    lgamma2 = torch.lgamma(n / 2)
    lgamma12 = torch.lgamma((n + 1) / 2)
    stmu_z = (
        lgamma12 - lgamma2 - ((n + 1)/2) * torch.log(1 + x2/n)
        - 0.5 * torch.log(n * Pi) - mu2/2
    )

    # A_v(x; mu) and B_v(x; mu) terms
    zterm = mu2x2 / (2 * (x2 + n))
    Av = loghyp1f1((df + 1)/2, .5, zterm)
    Bv = (
        torch.log(torch.sqrt(_2) * nc * x / torch.sqrt(x2 + n))
        + torch.lgamma(n/2 + 1) - lgamma12
        + loghyp1f1(df/2 + 1, 1.5, zterm)
    )

    pdf = torch.exp(stmu_z + Av) + torch.exp(stmu_z + Bv)
    pdf = torch.nan_to_num(pdf)
    if norm:
        pdf /= torch.sum(pdf)
        pdf = torch.nan_to_num(pdf)
    return torch.clamp(pdf, 0, 1).to(x.dtype)


def main():
    """Main function for obtaining distributions."""
    logging.basicConfig(encoding='utf-8', level=logging.WARNING)

    hr_df, hr_nc = 9, 52.36996
    sv_df, sv_nc = 9, 65.55
    co_df, co_nc = 80, 5.09

    # nct.stats returns mean, var, skew, kurtosis
    hr_mean, hr_var = nct.stats(hr_df, hr_nc, moments='mv')
    sv_mean, sv_var, sv_skew, sv_kurt = nct.stats(sv_df, sv_nc, moments='mvsk')
    co_mean, co_var, co_skew, co_kurt = nct.stats(co_df, co_nc, moments='mvsk')

    def gen_hr(N):
        return nct.rvs(hr_df, hr_nc, size=N)

    def calc_co(hr, sv):
        return (hr * sv) / 1000

    seed = np.random.randint(1000)
    print(f"Seed: {seed}")

    drn = distrel.drn(gen=gen_hr, calc=calc_co, seed=seed)

    # Final training based of probability distribution.
    budget = 1000
    N = 10000

    p = 1e-3
    sv_min = nct.ppf(p, sv_df, sv_nc)
    sv_max = nct.ppf(1-p, sv_df, sv_nc)
    co_min = nct.ppf(p, co_df, co_nc)
    co_max = nct.ppf(1-p, co_df, co_nc)

    @lru_cache
    def sv_pdf(quantiles):
        return nct_pdf(quantiles, sv_df, sv_nc)

    @lru_cache
    def co_pdf(quantiles):
        return nct_pdf(quantiles, co_df, co_nc)

    drn.set_properties(
        {"mean": hr_mean, "var": hr_var},
        {"mean": sv_mean, "var": sv_var, "pdf": sv_pdf, "min": sv_min, "max": sv_max},
        {"mean": co_mean, "var": co_var, "pdf": co_pdf, "min": co_min, "max": co_max},
    )

    optimisers = list(ng.optimizers.registry.keys())
    opt_results = dict()
    for i, key in enumerate(tqdm(optimisers)):
        try:
            _loss = drn.train(
                size=N,
                budget=budget,
                opt=ng.optimizers.registry[key],
                )
            opt_results[key] = _loss
        except:
            opt_results[key] = "N/A"

    with open("opts.json", "w") as f:
        json.dump(opt_results, f)

    # Gradient based training with metrics
    max_epochs = 100
    N = 10000
    lr = 1e-1

    drn.set_properties(
        {"mean": hr_mean, "var": hr_var},
        {"mean": sv_mean, "var": sv_var, "skew": sv_skew, "kurt": sv_kurt},
        {"mean": co_mean, "var": co_var, "skew": co_skew, "kurt": co_kurt},
    )
    converge = drn.train(
        max_epochs=max_epochs,
        progress_bar=True,
        size=N,
        lr=lr,
        require_closure=True,
        optim=LBFGS,
        optim_kwargs={'line_search_fn': 'strong_wolfe'},
    )

    print(f"Converged?:\t{converge}")
    print(drn)

    drn.dump("sv.toml")


if __name__ == "__main__":

    main()
