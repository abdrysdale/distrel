"""Tests for the distrel module."""

# Python imports
import os
import sys

# Module imports
import torch
from scipy.stats import nct

# Local imports
BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append(BASE_PATH)
import distrel


def test_drn():
    """Test the drn class."""
    toml_file = "test.toml"

    a_df, a_nc = 9, 52
    b_df, b_nc = 4, 30
    c_df, c_nc = 143, 90

    # nct.stats returns mean, var, skew, kurt
    a_mean, a_var, a_skew, a_kurt = nct.stats(a_df, a_nc, moments='mvsk')
    b_mean, b_var, b_skew, b_kurt = nct.stats(b_df, b_nc, moments='mvsk')
    c_mean, c_var, c_skew, c_kurt = nct.stats(c_df, c_nc, moments='mvsk')

    # We need to define a generater function that can generate samples of A.
    # It must take N (the number of samples as it's only argument.
    def gen_a(N):
        return nct.rvs(a_df, a_nc, size=N)

    # We also need to define a function that calculates $c$ from $a$ and $b$.
    # That is, the neural network will take input $a$, generate $b$ and then calculate $c$.
    def calc_c(a, b):
        return a * b

    drn = distrel.drn(gen=gen_a, calc=calc_c)
    drn.set_properties(
        {"mean": a_mean, "var": a_var, "skew": a_skew, "kurt": a_kurt},
        {"mean": b_mean, "var": b_var, "skew": b_skew, "kurt": b_kurt},
        {"mean": c_mean, "var": c_var, "skew": c_skew, "kurt": c_kurt},
    )

    drn.train(max_epochs=10, tol=1e-2)

    assert drn.dump(toml_file)

    # Tests jeffrey's divergence
    def b_pdf(quantiles):
        quantiles = quantiles.detach().cpu().numpy()
        return torch.tensor(nct.pdf(quantiles, b_df, b_nc))

    def c_pdf(quantiles):
        quantiles = quantiles.detach().cpu().numpy()
        return torch.tensor(nct.pdf(quantiles, c_df, c_nc))

    p = 1e-3
    b_min = nct.ppf(p, b_df, b_nc)
    b_max = nct.ppf(1-p, b_df, b_nc)
    c_min = nct.ppf(p, c_df, c_nc)
    c_max = nct.ppf(1-p, c_df, c_nc)

    drn = distrel.drn(gen=gen_a, calc=calc_c)
    drn.set_properties(
        {"mean": a_mean, "var": a_var},
        {"mean": b_mean, "var": b_var, "pdf": b_pdf, "min": b_min, "max": b_max},
        {"mean": c_mean, "var": c_var, "pdf": c_pdf, "min": c_min, "max": c_max},
    )
    drn.train(max_epochs=10, tol=1e-2)
    assert drn

    drn.load(toml_file)
    os.remove(toml_file)


if __name__ == "__main__":
    test_drn()
